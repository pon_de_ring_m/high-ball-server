#!/usr/bin/env python
# coding: UTF-8

#使い方
#python record.py filename

#出力
#/home/pi/high-ball-server/video/filename[年月日_時分秒].h264

import picamera
import ssl
import io
import sys
import os
import datetime

def write_pid(): #PID(プロセス識別子)書く 外部からこのプロセス終わるためにPID保存しとく
    f = open(PID_FILE, mode='w');
    f.write("%d" % os.getpid())
    f.close()

#datetimeクラスを使って日時取得
#フォーマット:年月日_時分秒　例:20150819_204517
d = datetime.datetime.today()
date = d.strftime("%Y%m%d_%H%M%S")

VIDEO_PATH="/home/pi/high-ball-server/video/" #ビデオファイルパス
PID_FILE = VIDEO_PATH + "video_pid" #PIDファイルパス

write_pid() #PIDファイル更新

filename = VIDEO_PATH + sys.argv[1] + date + ".h264"

#録画開始
camera = picamera.PiCamera()
try:
    camera.resolution = (640, 480)
    camera.start_recording(filename)
    camera.wait_recording(60) #デフォルトだと60秒で録画停止
    camera.stop_recording()

except:
    print("camera stop")
    camera.stop_recording()

finally:
    camera.close()
