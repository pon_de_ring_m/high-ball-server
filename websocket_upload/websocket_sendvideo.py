#!/usr/bin/env python
# coding: UTF-8

# Raspberry PI用の，Webカメラ画像をサーバーにWebsocket経由でアップロードするプログラムです
#
# 依存：
#   websocket-client : https://github.com/liris/websocket-client
#
# 注意：
#   ・ffmpegはソースコードからコンパイルしないとv4l2が使えない可能性があります
#   ・宛先アドレスは環境に合わせて書き換えてください
#
# 実行： python3 websocket_uploader.py
WIDTH = 480
HEIGHT = 360
FPS = 30

import subprocess
import ssl
import io
from websocket import create_connection
import time
import struct
import picamera
# import picamera

 # Connect to server
address = "ws://60.148.126.181:8124" #アドレス
ws = create_connection(address, sslopt={"cert_reqs": ssl.CERT_NONE})
print(ws.getstatus)

with picamera.PiCamera() as camera:
    camera.resolution = (WIDTH, HEIGHT)
#    camera.framerate = FPS
    stream = io.BytesIO()
    for foo in camera.capture_continuous(stream, format='jpeg'):
        # Truncate the stream to the current position (in case
        # prior iterations output a longer image)
        stream.truncate()
        stream.seek(0)
#	time.sleep(1)

        ws.send_binary(stream.read())
	stream.seek(0)
#        if process(stream):
#           break



# Upload

# # Recieve message
# while 1:
#     string = ws.recv()
#     print(string)


#encode_process.wait()
ws.close()
