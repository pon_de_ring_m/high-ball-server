#!/usr/bin/env python
# coding: UTF-8

# Raspberry PI用の，Webカメラ画像をサーバーにWebsocket経由でアップロードするプログラムです
#
# 依存：
#   websocket-client : https://github.com/liris/websocket-client
#
# 注意：
#   ・ffmpegはソースコードからコンパイルしないとv4l2が使えない可能性があります
#   ・宛先アドレスは環境に合わせて書き換えてください
#
# 実行： python3 websocket_uploader.py

import subprocess
import ssl
import io
from websocket import create_connection
import time
import struct
# import picamera


 # Connect to server
address = "ws://localhost:8124" #アドレス
ws = create_connection(address, sslopt={"cert_reqs": ssl.CERT_NONE})
print(ws.getstatus)
img = open("./cameratest.jpg","rb")

# カメラ撮影
# camera = picamera.PiCamera()
#
# camera.start_preview()
# time.sleep(5)
# camera.capture('cameratest.jpg')
# camera.stop_preview()
# camera.close()

# Upload
ws.send_binary(img.read())

# # Recieve message
# while 1:
#     string = ws.recv()
#     print(string)


#encode_process.wait()
ws.close()
