#!/usr/bin/env python
# coding: UTF-8

# Raspberry PI用の，Webカメラ映像をサーバーにWebsocket経由でアップロードするプログラムです
# 動かすと勝手にカメラ映像をアップロードし続けます
#
# 依存：
#   websocket-client : https://github.com/liris/websocket-client
#
# 注意：
#   ・ffmpegはソースコードからコンパイルしないとv4l2が使えない可能性があります
#   ・宛先アドレスは環境に合わせて書き換えてください
#
# 実行： python3 websocket_uploader.py

import subprocess
import ssl
import io
from websocket import create_connection

 # Connect to server
address = "ws://localhost:8124" #アドレス
ws = create_connection(address, sslopt={"cert_reqs": ssl.CERT_NONE})
print(ws.getstatus)
# Start ffmpeg
encode_process = subprocess.Popen([
    'ffmpeg',
    '-loglevel', 'panic',
    '-r', '15',
    '-f', 'v4l2',
    '-s', '320x240',
    '-i', '/dev/video0',
    '-c:a', 'none',
    '-f', 'mpegts',
    '-'
    ], stdin=None, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

# Upload
with io.open(encode_process.stdout.fileno(), closefd=False, mode='rb') as stream:
    while not stream.closed:
        ws.send_binary(stream.read(512))


# # Recieve message
# while 1:
#     string = ws.recv()
#     print(string)


#encode_process.wait()
ws.close()
