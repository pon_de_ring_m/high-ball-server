﻿#!/usr/bin/env python
# coding: UTF-8

# Raspberry PI用の，ステータスをサーバーにWebsocket経由でアップロードするプログラムです
# 第1引数にステータス名を指定します．
# 第1引数以降の文字列を送ります．
# 例：python websocket_sendstatus.py gps XXX YYY ZZZ
#
# 文字列にスペースが含まれていても連結して送れるように成りました
# todo スペースの数を保持するようにする
#
# 依存：
#   websocket-client : https://github.com/liris/websocket-client
#
# 注意：
#
#
# 実行： python3 websocket_sendstatus.py gpsstatus

import ssl
import io
from websocket import create_connection
import sys
import ConfigParser

params = sys.argv
length = len(params)
i = 1
str = []

if(length<2):
    print("ERROR!")
    quit()

#設定ファイル読み込み
config = ConfigParser.ConfigParser()
config.read("./config.ini")
address = config.get('default','address') + "?id=raspi&status="+params[1]
#address = "ws://ponderingm.iobb.net:8124?id=raspi&status=" + params[1] #村上家サーバー
#address = "ws://192.168.128.173:8124?id=raspi&status=" + params[1] #仲田ノートPC
#address = "ws://192.168.128.116:8124?id=raspi&status=" + params[1] #thinkpad red

 # Connect to server
#address = "ws://localhost:8124/?id=raspi&status="+params[1] #アドレス
ws = create_connection(address, sslopt={"cert_reqs": ssl.CERT_NONE})
#print(ws.getstatus)


# Send message
while i<length-1:
    str.append(params[i]+ " ")
    i+=1
str.append(params[i])
senddata = "".join(str)
ws.send(senddata)
#print(senddata)
ws.close()
