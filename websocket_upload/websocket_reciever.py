#!/usr/bin/env python
# coding: UTF-8

# Raspberry PI用の，Websocket経由でコマンドを受信するプログラムです
#
#
# 依存：
#   websocket-client : https://github.com/liris/websocket-client
#
# 注意：
#   ・ffmpegはソースコードからコンパイルしないとv4l2が使えない可能性があります
#   ・宛先アドレスは環境に合わせて書き換えてください
#
# 実行： python3 websocket_uploader.py

import subprocess
import ssl
import io
import traceback
import sys
import ConfigParser

#設定ファイル読み込み
config = ConfigParser.ConfigParser()
config.read('/home/pi/high-ball-server/websocket_upload/config.ini')
address = config.get('default','address') + "?id=raspi&status=other"
#address = "ws://ponderingm.iobb.net:8124?id=raspi&status=other"
#address = "ws://192.168.128.116:8124?id=raspi&status=other" #thinkpad red

from websocket import create_connection
while 1:
    try:
         # Connect to server
#        address = "ws://ponderingm.iobb.net:8124/?id=raspi" #アドレス
        ws = create_connection(address, sslopt={"cert_reqs": ssl.CERT_NONE})
        ws.send("raspi reciever start")

        # Recieve message
        while 1:
            string = ws.recv()
            if string != "":
                print(string)
            string = ""

        #encode_process.wait()
        ws.close()
    except KeyboardInterrupt, err:
        exit()

    except:
        # print "--------------------------------------------"
        # print traceback.format_exc(sys.exc_info()[2])
        # print "--------------------------------------------"
        pass

    finally:
        ws.close()
