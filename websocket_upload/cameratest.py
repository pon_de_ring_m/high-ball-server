# coding: utf-8
 
import time
import picamera
 
camera = picamera.PiCamera()
 
camera.start_preview()
time.sleep(1)
camera.capture('test.jpg')
camera.stop_preview()
camera.close()
