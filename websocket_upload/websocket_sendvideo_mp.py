#!/usr/bin/env python
# coding: UTF-8

# Raspberry PI用の，Webカメラ画像をサーバーにWebsocket経由でアップロードするプログラムです
# マルチプロセスで高速化しました
#
# 依存：
#   websocket-client : https://github.com/liris/websocket-client
#
# 注意：
#   ・ffmpegはソースコードからコンパイルしないとv4l2が使えない可能性があります
#   ・宛先アドレスは環境に合わせて書き換えてください
#
# 実行： python3 websocket_uploader.py
#address = "ws://ponderingm.iobb.net:8124" #アドレス

from multiprocessing import Process,Queue
import ssl
import io
from websocket import create_connection
import time
import struct
import picamera
import sys
import ConfigParser

#設定ファイル読み込み
config = ConfigParser.ConfigParser()
config.read('/home/pi/high-ball-server/websocket_upload/config.ini')
address = config.get('default','address') + "?id=raspi"
#address = "ws://ponderingm.iobb.net:8124?id=raspi"
#address = "ws://192.168.128.116:8124?id=raspi" #thinkpad red

def capture(q):
	camera = picamera.PiCamera()
	try:
		camera.resolution = (320, 240)
  	 	camera.framerate = 10
		stream = io.BytesIO()
		for foo in camera.capture_continuous(stream, format='jpeg'):
			stream.truncate()
			stream.seek(0)
			q.put(stream.read())
			stream.seek(0)
#            if process(self.stream):
#                break

	except RuntimeError:
		print("EROOR",sys.exc_info()[0])
		camera.close()
#	camera._check_camera_open()
#	camera = picamera.Picamera()
		pass
	finally:
		camera.close()



print("start program")
q = Queue(maxsize = 1)
p = Process(target=capture,args=(q,))
p.start()
while 1:
    try:
         # Connect to server
        ws = create_connection(address, sslopt={"cert_reqs": ssl.CERT_NONE})

        while 1:
            sendstream = q.get()
            ws.send_binary(sendstream)
    except KeyboardInterrupt, err:
        exit()

    except:
        pass

    finally:
        ws.close()
