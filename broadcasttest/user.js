var jwt = require('jwt-simple');
var secret = "hogefugabar";
var algorithm = 'HS512'
var users = require('./users.json');

module.exports = {
	encode: function(userid) {
		try{
			return jwt.encode(userid, secret, algorithm);
		}catch(e){
			console.log(e);
			return null;
		}
	},
	decode: function(token) {
		try{
			return jwt.decode(token, secret);
		}catch(e){
			console.log(e);
			return null;
		}
	},
	findById: function(id, fn) {
		var idx = id - 1;
		if (users[idx]) {
			fn(null, users[idx]);
		} else {
			fn(new Error('User ' + id + ' does not exist'));
		}
	},
	findByUsername: function(username, fn) {
		for (var i = 0, len = users.length; i < len; i++) {
			var user = users[i];
			if (user.username === username) {
				return fn(null, user);
			}
		}
		return fn(null, null);
	},
	findByToken: function(token, fn) {
		var dec_id = module.exports.decode(token, secret);
		module.exports.findById(dec_id, fn)
	}
}
