var User = require('./user');

module.exports = {
	strategy: function(){
		var LocalStrategy = require('passport-local').Strategy;
		return new LocalStrategy(
			{usernameField: "username", passwordField: "password"},
				function(username, password, done){
					process.nextTick(function(){
						User.findByUsername(username, function(err, user){
						if(err)
							return done(err);
						if(!user || user.password !== password)
							return done(null, false, {message: "Username and password do not match"});
						return done(null, user);
						});
					});
				}
			);
	},
	serializer: function(user, done){
		done(null, user.id);
	},
	deserializer: function(user, done){
		User.findById(user, function(err, user){
			done(err,user);
		})
	}
};


