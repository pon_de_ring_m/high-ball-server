var supportTouch = 'ontouchend' in document;

var goAheadBtn = document.getElementById('goahead');
var turnLBtn = document.getElementById('turnleft');
var turnRBtn = document.getElementById('turnright');
var goBackBtn = document.getElementById('goback');
var openCamBtn = document.getElementById('opencam');
var camFrontBtn = null;
var camTurnLBtn = document.getElementById('camturnleft');
var camTurnRBtn = document.getElementById('camturnright');
var wakingBtn = document.getElementById('waking');
var fallDownBtn = document.getElementById('falldown');
var emergencyBtn = document.getElementById('emergency');
var allBtn = document.getElementById('all');
var cam_actionflag = 0;

var reload_flag = false;
var flag;
var w_Btn_flag=true;

/*
function openCamera(){
	var input = document.createElement('input');
	input.type="button";
	input.className="campad";
	input.id="camfront";
	input.value="CAMERA\nFRONT";
	camFrontBtn = input;
	camFrontBtn.addEventListener((supportTouch?'touchstart':'mousedown'), function(){sendCmd('softcameraservo 15');});
	camFrontBtn.addEventListener((supportTouch?'touchend':'mouseup'), function(){sendCmd('softcameraservo stop');});
	document.getElementById("cam_center").removeChild(openCamBtn);
	document.getElementById("cam_center").appendChild(input);

	goAheadBtn.disabled=false;
	turnLBtn.disabled=false;
	turnRBtn.disabled=false;
	goBackBtn.disabled=false;
	camTurnLBtn.disabled=false;
	camTurnRBtn.disabled=false;
	wakingBtn.disabled=false;
	openCamBtn.value="CAMERA\nFRONT"
}
*/


//ミニマップ上のカメラの動き
function cameraTurn(pos, gps_d){
		//gps_df = (-1) * (gps_d);
	if(pos == 'center'){
		cam_actionflag = 0;
		$('#eye').css({ WebkitTransform:"rotate("+posDeg+"deg)"});
		$('#eye').css({ MozTransform:"rotate("+posDeg+"deg)"});
		$('#eye').css({ OTransform:"rotate("+posDeg+"deg)"});
		$('#eye').css({ msTransform:"rotate("+posDeg+"deg)"});
	}
	if(pos == 'left'){
		cam_actionflag = 1;
		$('#eye').css({ WebkitTransform:"rotate("+(posDeg - 45)+"deg)"});
		$('#eye').css({ MozTransform:"rotate("+(posDeg - 45)+"deg)"});
		$('#eye').css({ OTransform:"rotate("+(posDeg - 45)+"deg)"});
		$('#eye').css({ msTransform:"rotate("+(posDeg - 45)+"deg)"});
	}
	if(pos == 'right'){
		cam_actionflag = 1;
		$('#eye').css({ WebkitTransform:"rotate("+(posDeg + 45)+"deg)"});
		$('#eye').css({ MozTransform:"rotate("+(posDeg + 45)+"deg)"});
		$('#eye').css({ OTransform:"rotate("+(posDeg + 45)+"deg)"});
		$('#eye').css({ msTransform:"rotate("+(posDeg + 45)+"deg)"});
	}
}

//ボタンを押せなくする
function Btn_forbid(){
	goAheadBtn.disabled = true;
	turnLBtn.disabled = true;
	turnRBtn.disabled = true;
	goBackBtn.disabled = true;
	camTurnLBtn.disabled = true;
	camTurnRBtn.disabled = true;
	wakingBtn.disabled = true;
	fallDownBtn.disabled = true;
	emergencyBtn.disabled = true;
}

//ボタンを押せるようにする
function Btn_lifting(){
	goAheadBtn.disabled = false;
	turnLBtn.disabled = false;
	turnRBtn.disabled = false;
	goBackBtn.disabled = false;
	camTurnLBtn.disabled = false;
	camTurnRBtn.disabled = false;
	wakingBtn.disabled = false;
	fallDownBtn.disabled = false;
	emergencyBtn.disabled = false;
}


//ボタンを押したときに送られるコマンド
{
	goAheadBtn.addEventListener((supportTouch?'touchstart':'mousedown'), function(){
		if(time>0){
			sendCmd('motor go');
			flag=true;
			go_effect();
			reload_flag=true;
			}
		});
	turnLBtn.addEventListener((supportTouch?'touchstart':'mousedown'), function(){
		if(time>0){
			sendCmd('motor d');
			flag=true;
			left_effect();
			reload_flag=true;
			}
		});
	turnRBtn.addEventListener((supportTouch?'touchstart':'mousedown'), function(){
		if(time>0){
			sendCmd('motor a');
			flag=true;
			right_effect();
			reload_flag=true;
			}
		});
	goBackBtn.addEventListener((supportTouch?'touchstart':'mousedown'), function(){
		if(time>0){
			sendCmd('motor back');
			flag=true;
			back_effect();
			reload_flag=true;
			}
		});
	//openCamBtn.addEventListener('click', function(){sendCmd('softcameraservo 15');openCamera()});
	
	openCamBtn.addEventListener('click', function(){
		cameraTurn('center');
		sendCmd('softcameraservo 15');
		flag=true;
		});
	
	camTurnLBtn.addEventListener((supportTouch?'touchstart':'mousedown'),function(){
		if(time>0){
			cameraTurn('left',gps_d);
			sendCmd('softcameraservo 20');
			flag=true;
			camTurnL_effect();
			reload_flag=false;
			}
		});
	
	camTurnRBtn.addEventListener((supportTouch?'touchstart':'mousedown'),function(){
		if(time>0){
			cameraTurn('right',gps_d);
			sendCmd('softcameraservo 10');
			flag=true;
			camTurnR_effect();
			reload_flag=false;
			}
		});

	wakingBtn.addEventListener('click', function(){
		sendCmd('start waking');
		reload_flag=false;
		});
	fallDownBtn.addEventListener('click',function(){
		sendCmd('start rolloverreturn');
		reload_flag=false;
		});
	emergencyBtn.addEventListener('click', function(){
		sendCmd('testing videorequest');
		reload_flag=false;
		});
	
	allBtn.addEventListener((supportTouch?'touchend':'mouseup'),function(){
		if(flag){
			if(time>0){
				reload_flag=false
				sendCmd('motor stop');
				//reloadMap(gps_x,gps_y,posDeg);
				cameraTurn('center',posDeg);
				sendCmd('softcameraservo 15');
				flag=false;
				stop_effect();
			}
		}
	});
}

connect();
