var inf = document.createElement('p')
var inff = document.createElement('pp')
var infff = document.createElement('ppp')
var w_Btn_effect;
var f_Btn_effect;

//マップを生成中です
function creating_map(){
	inff.textContent="マップを生成中です("+String(flag_start)+"/20)"
	document.getElementById("check").appendChild(inff);
}
//マップの生成が完了しました
function complete_map(){
	inff.textContent="マップの生成が完了しました"
	document.getElementById("check").appendChild(inff);
}

//ローバーの現在の状況
function go_effect(){
	inf.textContent="前進中"
	document.getElementById("state").appendChild(inf);	
}
function left_effect(){
	inf.textContent="左旋回中"
	document.getElementById("state").appendChild(inf);
}
function right_effect(){
	inf.textContent="右旋回中"
	document.getElementById("state").appendChild(inf);
}
function back_effect(){
	inf.textContent="後進中"
	document.getElementById("state").appendChild(inf);
}
function stop_effect(){
	inf.textContent="待機中"
	document.getElementById("state").appendChild(inf);
}
function camTurnL_effect(){
	inf.textContent="左方確認中"
	document.getElementById("state").appendChild(inf);
}
function camTurnR_effect(){
	inf.textContent="右方確認中"
	document.getElementById("state").appendChild(inf);
}
function waking_effect(){
	inf.textContent="起き上がり"
	document.getElementById("state").appendChild(inf);
}
function falldown_effect(){
	inf.textContent="横転復帰"
	document.getElementById("state").appendChild(inf);
}

function waking_success(){
	inf.textContent="起き上がり成功"
	document.getElementById("state").appendChild(inf);
}
function waking_failure(){
	inf.textContent="起き上がり失敗"
	document.getElementById("state").appendChild(inf);
}

//横転復帰はメインで未実装なので、以下はUIとしても反映無し
function falldown_success(){
	inf.textContent="横転復帰成功"
	document.getElementById("state").appendChild(inf);
}
function falldown_failure(){
	inf.textContent="横転復帰失敗"
	document.getElementById("state").appendChild(inf);
}

//チェックポイント取得表示
function checkpoint1(){
	inff.textContent="１つめの部品を発見！"
	document.getElementById("check").appendChild(inff);
}
function checkpoint2(){
	$('#check').css("font-size", '2.5em');
	inff.textContent="２つめの部品を発見!"
	document.getElementById("check").appendChild(inff);
}
function goal(){
	$('#check').css("font-size", '2.5em');
	inff.textContent="３つめの部品を発見!!!"
	document.getElementById("check").appendChild(inff);
}
function score(){
	$('#check').css("font-size", '2.5em');
	inff.textContent="得点は " +  String(point) +" 点でした！"
	document.getElementById("check").appendChild(inff);
}
function messe(){
	$('#check').css("font-size", '2.5em');
	inff.textContent="thank you for playing"
	document.getElementById("check").appendChild(inff);
}

//得点の表示
function  goahead_first()//初めての前進
{
	$('#point_state').css("font-size",'1.8em');
	infff.textContent="初めて前進した　＋10pt"
	document.getElementById("point_state").appendChild(infff);
}
function goback_first()//初めての後進
{
	$('#point_state').css("font-size",'1.8em');
	infff.textContent="初めて後進した　＋10pt"
	document.getElementById("point_state").appendChild(infff);
}
function turnright_first()//初めての右折
{
	$('#point_state').css("font-size",'1.8em');
	infff.textContent="初めて右に曲がった　＋10pt"
	document.getElementById("point_state").appendChild(infff);
}
function turnleft_first()//初めての左折
{
	$('#point_state').css("font-size",'1.8em');
	infff.textContent="初めて左に曲がった　＋10pt"
	document.getElementById("point_state").appendChild(infff);
}
function camright_first()//初めての右方確認
{
	$('#point_state').css("font-size",'1.8em');
	infff.textContent="初めて右を見た　＋10pt"
	document.getElementById("point_state").appendChild(infff);
}
function camleft_first()//初めての左方確認
{
	$('#point_state').css("font-size",'1.8em');
	infff.textContent="初めて左を見た　＋10pt"
	document.getElementById("point_state").appendChild(infff);
}
function wakeup_first()//初めての起き上がり
{
	$('#point_state').css("font-size",'1.8em');
	infff.textContent="初めての起き上がり　＋20pt"
	document.getElementById("point_state").appendChild(infff);
}
function rolloverreturn_first()//初めての横転復帰
{
	$('#point_state').css("font-size",'1.8em');
	infff.textContent="初めての横転復帰　＋20pt"
	document.getElementById("point_state").appendChild(infff);
}
function videorequest_first()//初めての映像受信
{
	$('#point_state').css("font-size",'1.8em');
	infff.textContent="初めての映像の受信　＋30pt"
	document.getElementById("point_state").appendChild(infff);
}

function waking_p_3()//初めての映像受信
{
	$('#point_state').css("font-size",'1.8em');
	infff.textContent="起き上がりに成功した("+ String(p_w_flag) +"/3)　+20pt"
	document.getElementById("point_state").appendChild(infff);
}
function rolloverreturn_p_3()//初めての映像受信
{
	$('#point_state').css("font-size",'1.8em');
	infff.textContent="横転復帰に成功した("+ String(p_f_flag) +"/3)　+20pt"
	document.getElementById("point_state").appendChild(infff);
}


//ボタン点滅
function w_flash(){
	 w_Btn_effect = setInterval(function() {
		$('#wakeup').fadeOut(100, function() {
			$(this).fadeIn(100);
		});
	}, 200);
} 
function f_flash(){
	f_Btn_effect = setInterval(function() {
		$('#fall_down').fadeOut(100, function() {
			$(this).fadeIn(100);
		});
	}, 200);
}
function w_finish(){
	clearInterval(w_Btn_effect);
}
function f_finish(){
	clearInterval(f_Btn_effect);
}
