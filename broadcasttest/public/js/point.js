var point = 0;
var time = 0;//gps取得開始時からの経過時刻

var p_w_flag = 0;
var p_f_flag = 0;

var stop_point_flag = false;

function check1()//チェックポイント１到達
{
	point = point + 100;
}
function check2()//チェックポイント２到達
{
	point = point + 100;
}
function goal3()//ゴール到達
{
	point = point + 1000;
}

//mousedown時１回のみの判定
$("#top").one("mousedown",function()//初めての前進
{
	mi = 0;
	point = point + 10;
	goahead_first();
	stop_point_flag = true;
});
$("#bottom").one("mousedown",function()//初めての後進
{
	mi = 0;
	point = point + 10;
	goback_first();
	stop_point_flag = true;
});
$("#right").one("mousedown",function()//初めての右折
{
	mi = 0;
	point = point + 10;
	turnright_first();
	stop_point_flag = true;
});
$("#left").one("mousedown",function()//初めての左折
{
	mi = 0;
	point = point + 10;
	turnleft_first();
	stop_point_flag = true;
});
$("#cam_right").one("mousedown",function()//初めての右方確認
{
	mi = 0;
	point = point + 10;
	camright_first();
	stop_point_flag = true;
});
$("#cam_left").one("mousedown",function()//初めての左方確認
{
	mi = 0;
	point = point + 10;
	camleft_first();
	stop_point_flag = true;
});
$("#wakeup").one("mousedown",function()//初めての起き上がり
{
	mi = 0;
	point = point + 20;
	wakeup_first();
	stop_point_flag = true;
});
$("#fall_down").one("mousedown",function()//初めての横転復帰
{
	mi = 0;
	point = point + 20;
	rolloverreturn_first();
	stop_point_flag = true;
});
$("#emergency").one("mousedown",function()//初めての映像受信
{
	mi = 0;
	point = point + 30;
	videorequest_first();
	stop_point_flag = true;
});

//起き上がり、横転復帰成功時3回までポイント付与
function p_wakeup_success()//起き上がり成功
{
	if(p_w_flag <= 3){
		point = point + 20;
		mi = 0;
		waking_p_3();
		stop_point_flag = true;
	}
}
function p_rolloverreturn_success()//横転復帰成功
{
	if(p_f_flag <= 3){
		point = point + 20;
		mi = 0;
		rolloverreturn_p_3();
		stop_point_flag = true;
	}
}


function time_point()//クリアタイムでのボーナスポイント
{
	if(time <=  3600){
		point = point + (3600 - time);
	}
}
