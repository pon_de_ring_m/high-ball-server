var goalX = 0.000000, goalY = 0.000000; //初期値は2014年度ゴールGPS
var posX = 0.000000, posY = 0.000000;
var posView_X = 0.000000, posView_Y = 0.000000;
var goalDeg = 0, posDeg = 0;
var posArray = [[0,0],[0,0],[0,0]];
var mLat = 0.004472;//500m緯度
var mLong = 0.005861;//500m経度
var nLat = 0.001788;//200m緯度
var nLong = 0.002345;//200m経度
var oLat = 0.000089;//10m緯度
var oLong = 0.000117;//10m経度
var inc_flag;
var min_y, min_x;
var Max_y, Max_x;
var F_pointX, F_pointY;
var S_pointX, S_pointY;
var start_posX, start_posY;
var flag_start = 0;
var goalpos_x, goalpos_y;
var rateX, rateY,rateGX,rateGY,rateFX,rateFY,rateSX,rateSY;
var culX, culY,culGX,culGY,culFX,culFY,culSX,culSY;
var pos_D;
var flag1 = 0;
var flag2 = 0;
var flag3 = 0;
var flag4 = 0;
var t_oLong;
var t_oLat;
var test1, test2;
var w_X,w_Y;
var all_x=0;
var all_y=0;
var x_element = new Array(20);
var y_element = new Array(20);
var xx_element = [];
var yy_element = [];
var x_average =0, y_average=0;
var x_mini_ave ,x_max_ave;
var y_mini_ave ,y_max_ave;
var ii=0,kk=0,mm=0,iii=0,kkk=0,mmm=0;
var xx_all=0,yy_all=0;
var x_perf_all=0 ,y_perf_all=0;
var pos_1st = 0.000000, pos_2nd = 0.000000, pos_3rd = 0.000000;
var dif_pos;

var rocket_flag = false;
var stop_flag = false;
var messe_flag = false;

posArray[0][0] = 0;
posArray[0][1] = 0;

	function setGoal(x, y){
		goalX = x;
		goalY = y;
	}

	function reloadMap(y, x, pos){//ミニマップ更新用の関数
		posX = x / oLong;
		posY = y / oLat;
//		pos_D = (-1) * (pos);
		posArray[2][0] = posArray[1][0];
		posArray[2][1] = posArray[1][1];
		posArray[1][0] = posArray[0][0];
		posArray[1][1] = posArray[0][1];
		posArray[0][0] = posX;
		posArray[0][1] = posY;

		goalDeg = Math.atan2((goalpos_x / oLong) - posX, (goalpos_y / oLat) - posY)/(Math.PI / 180);
		$('#gnd').css({ WebkitTransform:"rotate("+goalDeg+"deg)"});
		$('#gnd').css({ MozTransform:"rotate("+goalDeg+"deg)"});
		$('#gnd').css({ OTransform:"rotate("+goalDeg+"deg)"});
		$('#gnd').css({ msTransform:"rotate("+goalDeg+"deg)"});

		posDeg = Math.atan2((posArray[0][0] - posArray[1][0]), (posArray[0][1] - posArray[1][1] ))/(Math.PI / 180);
		$('#navi').css({ WebkitTransform:"rotate("+ posDeg +"deg)"});
		$('#navi').css({ MozTransform:"rotate("+ posDeg +"deg)"});
		$('#navi').css({ OTransform:"rotate("+ posDeg +"deg)"});
		$('#navi').css({ msTransform:"rotate("+ posDeg +"deg)"});
		
		//以下はナビを絶対方向を基準に回転させる部分
//		$('#navi').css({ WebkitTransform:"rotate("+ pos_D +"deg)"});
//		$('#navi').css({ MozTransform:"rotate("+ pos_D +"deg)"});
//		$('#navi').css({ OTransform:"rotate("+ pos_D +"deg)"});
//		$('#navi').css({ msTransform:"rotate("+ pos_D +"deg)"});

		//以下はeyeをnaviと同じ方向に回転させる部分	
		$('#eye').css({ WebkitTransform:"rotate("+ posDeg +"deg)"});
		$('#eye').css({ MozTransform:"rotate("+ posDeg +"deg)"});
		$('#eye').css({ OTransform:"rotate("+ posDeg +"deg)"});
		$('#eye').css({ msTransform:"rotate("+ posDeg +"deg)"});
	}

	function reloadViewMap(y,  x){//俯瞰図マップ更新
		//------------------------現在地更新
		posView_X  = x;
		posView_Y  = y;
		//end---------------------現在値更新
		
		if(flag_start <20){
			all_x = all_x + x; //xの20回分の合計値をとる
			all_y = all_y + y; //yの20回分の合計値をとる
			x_element[flag_start] = x;  //xの配列の要素にそれぞれ取得したxの値を代入する
			y_element[flag_start] = y;  //yの配列の要素にそれぞれ取得したyの値を代入する
		}

		if(flag_start == 20){  //スタート地点の初期値を決めます
			//全体の平均値をとる
			x_average = all_x / 20;
			y_average = all_y / 20;

			//平均値からある範囲で値を評価する。それ以外はノイズとして除外する。
			/*現verは200m誤差の評価範囲*/
			x_mini_ave = x_average - nLong;
			x_max_ave = x_average +nLong;

			y_mini_ave = y_average - nLat;
			y_max_ave = y_average + nLat;

			//averageの許容範囲の計算を行う
			//ノイズ除去
			for(ii=0 ; ii<20 ; ii++){
				if(x_mini_ave < x_element[ii] && x_element[ii] < x_max_ave){
					xx_all = xx_all + x_element[ii];　//ノイズ除去後の合計値
					kk = kk+1; //ノイズ除去後の要素の数
				}
			}

			for(iii=0 ; iii<20 ; iii++){
				if(y_mini_ave < y_element[iii] && y_element[iii] < y_max_ave){
					yy_all = yy_all + y_element[iii];　//ノイズ除去後の合計値
					kkk = kkk+1; //ノイズ除去後の要素の数
				}
			}

			//ノイズを除去したkk回分の値の平均値を算出する
			start_posX = xx_all / kk;
			start_posY = yy_all  / kkk;


/*
		//-----------初期設定
		if(flag_start == 15){	//１回目にこの関数を呼び出したときのみ
		//スタートの位置代入
		start_posX = x;
		start_posY = y;
*/

		/*書き換えポイント１　ゴールの位置を指定  100m ver*/
		goalpos_x = x;
		goalpos_y = y + (10 * oLat);
		
		/*書き換えポイント2  チェックポイント１の位置を指定  100m ver*/
		F_pointX = start_posX + (3 * oLong);
		F_pointY = start_posY + (3 * oLat);
		
		/*書き換えポイント3　チェックポイント２の位置を指定　100m ver*/
		S_pointX = start_posX - (2 * oLong);
		S_pointY = start_posY + (6 *  oLat);
		
		/*書き換えポイント４　探索範囲の指定　100m ver*/
		Max_x = start_posX + (8 * oLong);
		min_x = start_posX - (8 * oLong);
		Max_y = start_posY + (12 * oLat);
		min_y = start_posY - (2 * oLat);

		culFX = F_pointX - min_x;
		culSX = S_pointX - min_x;
		culGX = goalpos_x - min_x;

		w_X = Max_x - min_x;
		w_Y = Max_y - min_y;


		rateFX = (culFX / w_X) * 178 + 1;
		rateFY = ((F_pointY - min_y) / w_Y) * 130 + 2;
		rateSX = (culSX / w_X) * 178 +1;
		rateSY = ((S_pointY - min_y) / w_Y) * 130 + 2;
		rateGX =(culGX / w_X) * 178 + 1;
		rateGY = ((goalpos_y - min_y) / w_Y) *130 + 2;


		$('#goal').css("bottom", rateGY);
		$('#goal').css("left", rateGX);		
		$('#point1').css("bottom", rateFY);
		$('#point1').css("left", rateFX);		
		$('#point2').css("bottom", rateSY);
		$('#point2').css("left", rateSX);		
		
		$('#point1').css("max-width", '100%');
		$('#point1').css("max-height", '100%');

		}
		//if-end-------初期設定
		
		
		//-------------------------具体的な更新の記述
		culX = posView_X - min_x;

		rateX = (culX/w_X) * 178 + 1;	//相対的に現在地を更新
		rateY = ((posView_Y - min_y)/w_Y) * 130 + 2; 	//相対的に現在地を更新

		$('#posit').css("bottom", rateY);
		$('#posit').css("left", rateX);

		
		//end---------------------具体的な更新の記述
		
		
		flag_start = flag_start + 1;	//呼び出した回数の確認フラグ

	}
	
	function decision(y , x){	//ポイント判定の関数

		//----------------第一チェックポイント到着した場合
		t_oLong = oLong / 2;
		t_oLat = oLat / 2;
		if(flag1 != 1){
		if((x > (F_pointX - t_oLong)) && (y > (F_pointY - t_oLat))){
			if((x < (F_pointX + t_oLong)) && (y < (F_pointY + t_oLat))){
			//window.alert('チェックポイント１に到達しました');
			console.log('チェックポイント１に到達しました');
			checkpoint1();//画面に表示--->effect.js
			//チェックポイント２の画像表示
			$('#check').css("font-size", '2.5em');
			$('#point2').css("max-width", '100%');
			$('#point2').css("max-height", '100%');
			$('#parts1').css("max-width", '10%');
			$('#parts1').css("max-height", '10%');
			flag1 = 1;
			ri = 0;
			stop_flag = true;
			check1(); //100ptをゲット--->point.js
			}
		}
		}
		//end-------------第一チェックポイント到達
		
		//-----------------第二チェックポイント到達
		if(flag2 != 1){
		if((x > (S_pointX - t_oLong)) && (y > (S_pointY - t_oLat))){
			if((x < (S_pointX + t_oLong)) && (y < (S_pointY + t_oLat))){
			//window.alert('チェックポイント２に到達しました');
			console.log('チェックポイント２に到達しました');
			checkpoint2();//画面に表示--->effect.js
			//ゴール画像の表示
			$('#check').css("font-size", '2.5em');
			$('#goal').css("max-width", '100%');
			$('#goal').css("max-height", '100%');
			$('#parts2').css("max-width", '10%');
			$('#parts2').css("max-height", '10%');
			flag2 = 1;
			ri = 0;
			stop_flag = true;
			check2(); //100ptをゲット--->point.js
			}
		}
		}
		//end-------------第二チェックポイント到達
		
		//-----------------ゴール到達
		if(flag3 != 1){
		if((x > (goalpos_x - t_oLong)) && (y > (goalpos_y - t_oLat))){
			if((x < (goalpos_x + t_oLong)) && (y < (goalpos_y + t_oLat))){
			//window.alert('ゴールに到達しました');
			console.log('ゴール周辺です');
			$('#check').css("font-size", '2.5em');
			$('#parts3').css("max-width", '10%');
			$('#parts3').css("max-height", '10%');
			goal();//画面に表示--->effect.js
			flag3 = 1;
			ri = 0;
			stop_flag = true;
			goal3();//100ptをゲット--->point.js
			time_point();//得点に時間ボーナスを追加--->point.js
			}
		}
		}

		if(flag4!=1 && rocket_flag == true){
			$('#parts1').css("max-width", '0%');
			$('#parts1').css("max-height", '0%');
			$('#parts2').css("max-width", '0%');
			$('#parts2').css("max-height", '0%');
			$('#parts3').css("max-width", '0%');
			$('#parts3').css("max-height", '0%');
			$('#rocket').css("max-width", '30%');
			$('#rocket').css("max-height", '30%');	
			score();//得点を画面に表示する--->effect.js
			stop_flag = true;
			flag4=1;
		}

		if(flag4==1 && messe_flag==true)
		{
			$('#launch img').css("animation-name",'anime');
			$('#launch img').css("-webkit-animation-name",'anime');	
			$('#launch img').css("-ms-animation-name",'anime');
			$('#launch img').css("-o-animation-name",'anime');
			$('#launch img').css("-moz-animation-name",'anime');
			
			messe();//メッセージを画面に表示する--->effect.js
		}
		//end-------------ゴール到達
		
	}
	
//開発中

function mileage(y,x){
	/* 2点間GPSの差から走行距離を計算する */
	/* １秒ごとにするか、２秒ごとにするか・・・要検証 */
	/*var oLat = 0.000089;//10m緯度
var oLong = 0.000117;//10m経度*/
/* 1m緯度 0.000009（プラスで北）
1m経度　0.000012　（プラスで東）
	*/
	pos_3rd = pos_2nd;
	pos_2nd = pos_1st;
	pos_1st = x;
			
	dif_pos = pos_3rd - pos_1st; //GPSの誤差
	
	/* 緯度と経度を計算する */
	
	
	
}