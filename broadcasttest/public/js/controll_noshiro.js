var supportTouch = 'ontouchend' in document;

var goAheadBtn = document.getElementById('goahead');
var turnLBtn = document.getElementById('turnleft');
var turnRBtn = document.getElementById('turnright');
var goBackBtn = document.getElementById('goback');
var openCamBtn = document.getElementById('opencam');
var camFrontBtn = null;
var camTurnLBtn = document.getElementById('camturnleft');
var camTurnRBtn = document.getElementById('camturnright');
var paraSepaBtn = document.getElementById('sepapara');
var emergencyBtn = document.getElementById('emergency');

function openCamera(){
	var input = document.createElement('input');
	input.type="button";
	input.className="campad";
	input.id="camfront";
	input.value="CAMERA\nFRONT";
	camFrontBtn = input;
	camFrontBtn.addEventListener((supportTouch?'touchstart':'mousedown'), function(){sendCmd('softcameraservo 15');});
	camFrontBtn.addEventListener((supportTouch?'touchend':'mouseup'), function(){sendCmd('softcameraservo stop');});
	document.getElementById("cam_center").removeChild(openCamBtn);
	document.getElementById("cam_center").appendChild(input);

	goAheadBtn.disabled=false;
	turnLBtn.disabled=false;
	turnRBtn.disabled=false;
	goBackBtn.disabled=false;
	camTurnLBtn.disabled=false;
	camTurnRBtn.disabled=false;
	paraSepaBtn.disabled=false;
	openCamBtn.value="CAMERA\nFRONT"
}
function cameraTurn(pos){
	if(pos == 'center'){
		$('#eye').css({ WebkitTransform:"rotate("+0+"deg)"});
		$('#eye').css({ MozTransform:"rotate("+0+"deg)"});
		$('#eye').css({ OTransform:"rotate("+0+"deg)"});
		$('#eye').css({ msTransform:"rotate("+0+"deg)"});
	}
	if(pos == 'left'){
		$('#eye').css({ WebkitTransform:"rotate("+315+"deg)"});
		$('#eye').css({ MozTransform:"rotate("+315+"deg)"});
		$('#eye').css({ OTransform:"rotate("+315+"deg)"});
		$('#eye').css({ msTransform:"rotate("+315+"deg)"});
	}
	if(pos == 'right'){
		$('#eye').css({ WebkitTransform:"rotate("+45+"deg)"});
		$('#eye').css({ MozTransform:"rotate("+45+"deg)"});
		$('#eye').css({ OTransform:"rotate("+45+"deg)"});
		$('#eye').css({ msTransform:"rotate("+45+"deg)"});
	}
}

goAheadBtn.addEventListener((supportTouch?'touchstart':'mousedown'), function(){cameraTurn('center');sendCmd('softcameraservo 15');sendCmd('motor go')});
goAheadBtn.addEventListener((supportTouch?'touchend':'mouseup'), function(){sendCmd('motor stop')});
turnLBtn.addEventListener((supportTouch?'touchstart':'mousedown'), function(){sendCmd('motor d')});
turnLBtn.addEventListener((supportTouch?'touchend':'mouseup'), function(){sendCmd('motor stop')});
turnRBtn.addEventListener((supportTouch?'touchstart':'mousedown'), function(){sendCmd('motor a')});
turnRBtn.addEventListener((supportTouch?'touchend':'mouseup'), function(){sendCmd('motor stop')});
goBackBtn.addEventListener((supportTouch?'touchstart':'mousedown'), function(){cameraTurn('center');sendCmd('softcameraservo 15');sendCmd('motor back')});
goBackBtn.addEventListener((supportTouch?'touchend':'mouseup'), function(){sendCmd('motor stop')});
//openCamBtn.addEventListener('click', function(){sendCmd('softcameraservo 15');openCamera()});
openCamBtn.addEventListener('click', function(){cameraTurn('center');sendCmd('softcameraservo 15')});
camTurnLBtn.addEventListener('click', function(){cameraTurn('left');sendCmd('softcameraservo 20')});
camTurnRBtn.addEventListener('click', function(){cameraTurn('right');sendCmd('softcameraservo 10')});
paraSepaBtn.addEventListener('click', function(){sendCmd('start waking')});
emergencyBtn.addEventListener('click', function(){sendCmd('motor stop');sendCmd('softcameraservo stop')});

connect();
