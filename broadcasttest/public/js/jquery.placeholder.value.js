﻿/* Information
----------------------------------------------
File Name : jquery.placeholder.value.js
URL : http://www.atokala.com/
Copyright : (C)atokala
Author : Masahiro Abe
--------------------------------------------*/
(function($){
	$.fn.ATFormFocus = function(config) {

		var defaults = {
			emptyClass : 'empty'
		}

		var options = $.extend(defaults, config);

		return this.each(function(){

			function setFocusBlur(input) {
				input.onfocus = function(){hidden(this)};
				input.onblur = function(){view(this);}
			}

			function hidden(input) {
				if (input.value == input.defaultValue) {
					input.value = '';
					$(input).removeClass(options.emptyClass);
				}
			}

			function view(input) {
				if (input.value == '') {
					input.value = input.defaultValue;
					$(input).addClass(options.emptyClass);
				}
			}

			var input = this;
			if (input.type == 'text' || input.type == 'textarea') {
				hidden(input);
				view(input);
				setFocusBlur(input);
			}
		});
	};
})(jQuery);
