var ws = null;
var gps_status;
var gps_s,gps_x,gps_y,gps_z; //衛星数,x,y,z,方角
var gps_d;
var func;
var i=0;
var j=0;
var k=0;
var l=0;
var ri=0;
var mi=0;
var judge_waking;
var judge_rolloverreturn;
var Btn_forbid_flag = false;

/*toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}*/

function onend() {
	toastr["warning"]("Server Disconnected", "WARNING");
	setTimeout(connect, 5000 );
//	document.location = "http://" + location.host;
}

function onsocketerror(event){
	toastr["error"]("Server Connect Failed", "ERROR");
	setTimeout(connect, 5000 );
}


//新規開発項目

function connect(){
	var key=null, message='messages', image='camera';
	ws = new WebSocket("ws://" + location.host + "/?token=" + key + "&id=commander");
	ws.onmessage = function(e) {
		var p = document.createElement('p');
		var data = e.data;
		if (data.constructor === String) {
			if (data.indexOf("rover_status") != -1)//ボタン解放
			{
				Btn_lifting();
	  	   	  	rover_status = data.split(" ");
				time++;
				
        		gps_s = parseInt(rover_status[2]);
        		gps_x = parseFloat(rover_status[3]);
        		gps_y = parseFloat(rover_status[4]);
        		gps_z = parseFloat(rover_status[5]);
        		gps_d = parseFloat(rover_status[6]); //direction 方角
				judge_waking = parseInt(rover_status[7]);
				judge_rolloverreturn = 0;
				if(judge_waking == 1 && parseInt(rover_status[8]) == 1){
					judge_waking = 0;
					judge_rolloverreturn = 1;
				}
				decision(gps_x, gps_y);
				
				if(gps_s > 3)//gpsの値が4以上の時
				{
					if(cam_actionflag != 1 && reload_flag == true)
					{
						reloadMap(gps_x, gps_y, gps_d);//ミニマップ更新
					}
					reloadViewMap(gps_x,gps_y);//俯瞰図更新
				}
				if(flag_start <= 20){
					creating_map();
				}else if(flag_start == 21){
					complete_map();
					stop_flag = true;
					ri = 0;
				}
				if(flag_start >= 21){
					mileage (gps_x,gps_y)
				}
					
	       	 	console.log(data); //コンソールにログを出力

				p.id = 'catch';
				if(gps_s <= 3)//衛星取得数が3以下の時
				{
					p.textContent = ("衛星数は " + rover_status[2] + "、 "  + "現在地を取得中です。"  );
				}
				else{
					p.textContent = ("衛星数は " + rover_status[2] + "、 " + "現在地は" + "　x=" +rover_status[3] + "　y= " +rover_status[4] + "　です。");
				}

				document.getElementById(message).appendChild(p);
				document.getElementById(message).scrollTop = document.getElementById(message).scrollHeight;
				if(stop_flag == true)//チェックポイント・ゴール取得表示--->update.js
				{
					ri = ri + 1;
						if(ri == 6)//6秒たったら表示終了
						{
							ri = 0;
							stop_flag = false;
							$('#check').css("font-size", '0em');
							if(flag3==1){
								rocket_flag=true;
							}
							if(flag4==1){
								messe_flag=true;
							}
						}
				}	
				if(stop_point_flag == true)//得点取得表示--->effect.js  point.js
				{
					mi = mi + 1;
					if(mi == 6)//6秒たったら表示終了
					{
						mi = 0;
						stop_point_flag = false;
						$('#point_state').css("font-size", '0px');
					}
				}
				if(Btn_forbid_flag == true)//ボタン禁止フラグが立っていたら...
				{
					//ボタン禁止フラグは起き上がりの時のみ立つので（横転復帰は未実装のため）
					waking_effect();
					Btn_forbid();//ボタンロック
					w_finish();//起き上がり点滅終了-->effect.js
					f_finish();//横転復帰点滅終了
				}
				if(Btn_forbid_flag == false)
				{
					Btn_lifting();//ボタン解除
				}
				//起き上がり点滅判定
				if(judge_waking == 1 && Btn_forbid_flag == false){
					i=i+1;
					j=0;
					if(i>=7){//7回連続でjudge_wakingのフラグがたったら
						w_finish();//起き上がり点滅終了
						w_flash();//起き上がり点滅開始
						f_finish();//横転復帰点滅終了
					}
				}
				if(judge_waking == 0){
					i=0;
					j=j+1;
					if(j>=3){
						w_finish();//起き上がり点滅終了
					}
				}

				//横転復帰点滅判定
				if(judge_rolloverreturn == 1 && Btn_forbid_flag == false){
					k=k+1;
					l=0;
					if(k>=7){//7回連続でjudge_rolloverreturnのフラグがたったら
						f_finish();//点滅終了
						f_flash();//点滅開始
					}
				}
				if(judge_rolloverreturn == 0){
					k=0;
					l=l+1;
					if(l>=3){
						f_finish();//点滅終了
					}
				}

			}
			if (data.indexOf("start waking") != -1)
			{
				waking_effect();
				w_finish();//起き上がり点滅終了-->effect.js
				Btn_forbid();//ボタンロック開始
				Btn_forbid_flag = true;
				i=0;
			}
			if (data.indexOf("start rolloverreturn") != -1)
			{
				f_finish();//横転復帰点滅終了-->effect.js
/*メインの実装が間に合わなかったので、バグ回避のためにボタンロックは無しとします*/
				//Btn_forbid();//ボタンロック開始
				//Btn_forbid_flag = true;
				k=0;
			}

			if (data.indexOf("waking succeed") != -1)
			{
				waking_success();//起き上がり成功表示-->effect.js
				Btn_lifting();//ボタンロック終了
				Btn_forbid_flag = false;
				p_w_flag = p_w_flag + 1;
				p_wakeup_success();
			}
			if (data.indexOf("waking faild")	!= -1)
			{
				waking_failure();//起き上がり失敗表示-->effect.js
				
				Btn_lifting();//ボタンロック終了
				Btn_forbid_flag = false;
			}
			if (data.indexOf("rolloverreturn succeed") != -1)
			{
				falldown_success();//横転復帰成功表示-->effect.js
				Btn_lifting();//ボタンロック終了
				Btn_forbid_flag = false;
				p_f_flag = p_f_flag + 1;
				p_rolloverreturn_success();
			}
			if (data.indexOf("rolloverreturn faild") != -1)
			{
				falldown_failure();//横転復帰失敗表示-->effect.js
				Btn_lifting();//ボタンロック終了
				Btn_forbid_flag = false;
			}

			if (data.indexOf("cmd") != -1) //コマンドの時ループバック
			{
				if (data.indexOf("test") != -1)
				{
					func = data.split(" ");
					eval(func[2]);
					p.id = 'test';
		  			p.textContent = data;
		  			document.getElementById(message).appendChild(p);
		  			document.getElementById(message).scrollTop = document.getElementById(message).scrollHeight;
				}
				else if (data.indexOf("rover_status") == -1)
				{
					p.id = 'sent';
	  				p.textContent = data;
	  				document.getElementById(message).appendChild(p);
	  				document.getElementById(message).scrollTop = document.getElementById(message).scrollHeight;
				}
			}
		} else if (data.constructor === Blob) {
				var img = new Image();
				img.src = URL.createObjectURL(data);
				document.getElementById(image).src=img.src;
		}
	}

	ws.onclose = onend;
//	ws.onerror= onsocketerror(event);
}


function sendText() {
	var text = document.getElementById('message')
	ws.send(text.value);
	text.value = "";
}
$('#message').ATFormFocus({
    emptyClass : 'empty'
});

function sendCmd(cmd) {
	ws.send(cmd);
}
function createXMLHttpRequest() {
	if(window.XMLHttpRequest) {
		return new XMLHttpRequest();
	}else if(window.ActiveXObject) {
		try {
			return new ActiveXObject("Msxml2.XMLHTTP");
		}catch(e){}
		try{
			return new ActiveXObject("Microsoft.XMLHTTP");
		}catch(e){}
		return null;
	}
}
function connectWithAuth(message,image){
	//token request
	var req = createXMLHttpRequest();
	req.open("GET", "http://" + location.host + "/get_token", true);
	req.onload = function(ev) {
		connect(req.response, message, image);
	}
	req.send();
}
