/**
 * Module dependencies.
 */

var express = require('express')
, routes = require('routes')
, http = require('http')
, path = require('path')
, User = require('./user')
, local = require('./passport-local-static')
, broadcast = require('./broadcast-server');

// passportで必要なもの
var flash = require("connect-flash")
, passport = require("passport");

// passport
passport.serializeUser(local.serializer);
passport.deserializeUser(local.deserializer);
passport.use(local.strategy());

var app = express();

// all environments
app.set('port', process.env.PORT || 8124);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser());
app.use(express.session({secret: "another_random_sevret_again"})); // こちらにも別のシークレットが必要です

// app.router を使う前にpassportの設定が必要です
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
	app.use(express.errorHandler());
}
// リクエストがあったとき、ログイン済みかどうか確認する関数
function isLogedIn(req, res, next){
	if(req.isAuthenticated())
		return next(); // ログイン済み
	// ログインしてなかったらログイン画面に飛ばす
	res.redirect("/login");
}

app.get('/', function(req, res){
	res.render("index", {user: req.user});
});
app.get("/login", function(req, res){
	res.render("login", {user: req.user, message: req.flash("error")});
});
app.post("/login",
		passport.authenticate("local", {failureRedirect: '/login', failureFlash: true}),
		function(req, res){
			// ログインに成功したらリダイレクト
			res.redirect("/");
		});
app.get("/logout", function(req, res){
	req.logout();
	res.redirect("/");
});
app.get("/member_only", isLogedIn, function(req, res){
	res.render("member_only", {user: req.user});
});
app.get("/uploader.html", isLogedIn);
app.get("/get_token", isLogedIn, function(req, res){
	res.send(User.encode(req.user.id));
});
var httpServer = http.createServer(app);
httpServer.listen(app.get('port'), function(){
	console.log('Express server listening on port ' + app.get('port'));
}); 
broadcast.create(httpServer);

