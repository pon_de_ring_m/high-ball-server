var User = require('./user');
module.exports = {
	create: function(httpServer){
		var WSServer = require('websocket').server;
		var webSocketServer = new WSServer({httpServer: httpServer, maxReceivedFrameSize: 10 * 1024 * 1024});
		var Url = require('url');
		webSocketServer.on('request', function (req) {
			var websocket = req.accept(null, req.origin);

			connections_all.push(websocket);
			var isAuthed = false;
			var token = Url.parse(req.resourceURL).query['token'];
			if(token){
				User.findByToken(token, function(user){
					isAuthed = true;
				})
			}
			var id = Url.parse(req.resourceURL).query['id'];
			var status = Url.parse(req.resourceURL).query['status'];
			if(id=="raspi")
			{
				connections_raspi.push(websocket);
			}else if(id == "commander")
			{
				connections_commander.push(websocket);
			}

			websocket.on('message', function(msg) {
				console.log("Msg type: " + msg.type);
				if (msg.type === 'utf8') {
					if (id =="raspi") //ラズパイからのメッセージ
					{
						console.log('"' + msg.utf8Data + '" is recieved from ' + req.origin + " id:" + id + " status:" + status +'!');

						if (status == "gps") //ステータスGPS
						{
							module.exports.broadcastUTF_Commander(msg.utf8Data);
						}
						else { //それ以外
							module.exports.broadcastUTF_Commander("other "+ msg.utf8Data);
						}

					} else if (id =="commander") //PCからのメッセージ
					{
						console.log('"' + msg.utf8Data + '" is recieved from ' + req.origin + " id:" + id +'!');
						module.exports.broadcastUTF_Raspi(msg.utf8Data); //���Y�p�C�ɃR�}���h���M
						module.exports.broadcastUTF_Commander("cmd "+ msg.utf8Data);

					}
					// module.exports.broadcastUTF(msg.utf8Data);
				} else if (msg.type === 'binary') {
					module.exports.broadcastBytes_Commander(msg.binaryData);

				}
			});

			websocket.on('close', function (code,desc) {
				console.log('connection released! :' + code + ' - ' + desc);

				var index = connections_all.indexOf(connections_all);
				if (index !== -1) {
					connections_all.splice(index, 1)
				}
			});
		});
		console.log('Broadcast-server: initialized');
	},
	// broadcastUTF: function(message) {
	// 	connections.forEach(function (con, i) {
	// 		con.sendUTF(message);
	// 	});
	// },
	broadcastUTF_Raspi: function(message) {
		connections_raspi.forEach(function (con, i) {
			con.sendUTF(message);
		});
	},
	broadcastUTF_Commander: function(message) {
		connections_commander.forEach(function (con, i) {
			con.sendUTF(message);
		});
	},
	broadcastBytes: function(message) {
		connections.forEach(function (con, i) {
			con.sendBytes(message);
		});
	},
	broadcastBytes_Commander: function(message) {
		connections_commander.forEach(function (con, i) {
			con.sendBytes(message);
		});
	}

}
var connections_all=[];
var connections_raspi = [];
var connections_commander = [];
