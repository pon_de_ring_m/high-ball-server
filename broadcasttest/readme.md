#何これ
べんぬ氏製作のNode.js用の画像配信サーバーです。
Websocketを使って，リアルタイムにアップロードされた画像を配信します。
アップロードするには，ログインする必要があります

#使い方
1. cd broadcasttest
2. npm start

npm startしたサーバーにブラウザでつなぐ
例：localhost:8124

#認証
ログインユーザー名とパスワードはusers.jsonに書いてあります

#broadcast-server.js
websocketサーバー

#pasport-local-static
ログイン関連

#app.js
サーバーのメインプログラム