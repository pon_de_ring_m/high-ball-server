#開発環境入れる （node.js）
http://www.amelt.net/imc/programming/nodejs/3464/

node.js

#分担
* 村上：サーバー→ローバー コマンド送信

* 森本さん：ブラウザUI設計

#その後
* ユーザー → サーバー → ローバー コマンド送信テスト ←できた

* サーバー → ユーザー 画像送信テスト

#使い方

``` 
~/high-ball-server/start.sh
```

#止め方

``` 
sudo ~/high-ball-server/stop.sh
```